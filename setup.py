import setuptools
from cli_framework import __doc__, __version__

setuptools.setup(
    name = "cli_framework",
    packages = setuptools.find_packages(),
    version = __version__,
    description = "Python framework to create command line tools.",
    long_description = __doc__,
    author = "Sylvan LE DEUNFF",
    author_email = "sledeunf@gmail.com",
    url = "http://pypi.sylvan.ovh/ngx"
)
