import os, json
from sys import argv
from collections import defaultdict
from .exc import CLIException, InvalidParameter, UnknownCommand, Documentation
from .commands import ConfigCMD, HelpCMD

# cli object
class CLI:
    """
    DOCSTRING FOR CLI PROGRAM
    """

    commands = {}

    def __init__(self, name, version=None):
        self.name = name
        self.version = version
        self.add_command(ConfigCMD, 'cfg', 'config')
        self.add_command(HelpCMD, 'help', '--help', '/?', '?')

    def add_command(self, Command, *commands):
        for command in commands:
            self.commands[command] = Command

    def load_config(self):
        try:
            self.config = json.load(open(os.path.join(os.path.expanduser('~'), '.ngxrc')))
        except FileNotFoundError:
            self.config = {}

    def documentation(self):
        print(self.__doc__)

    def parse_argv(self, Command, argv):
        for _ in (0,1): argv.pop(0)
        flag, args, kwargs = 0, (), defaultdict(bool)
        while argv:
            arg = argv.pop(0)
            if arg.startswith('--'):
                kwargs[arg[2:]] = True
            elif arg.startswith('-'):
                try:
                    key = Command.shortcuts[arg]
                except KeyError:
                    raise InvalidParameter('Invalid parameter "%s".'%arg)
                flag = 2
            elif flag == 1:
                kwargs[key] = arg
            else:
                args += (arg,)
            flag -= 1*(flag>0)
        return args, kwargs

    def process_command(self):
        try:
            if len(argv) <= 1:
                self.documentation()
            elif not self.commands.get(argv[1]):
                raise UnknownCommand('command "%s" does not exists.'%argv[1])
            else:
                self.load_config()
                Command = self.commands[argv[1]]
                args, kwargs = self.parse_argv(Command, argv)
                Command.process(self.config, *args, **kwargs)
        except Documentation:
            self.documentation()
        except CLIException as e:
            print(e)
