# base exception
class CLIException(Exception):
    pass

# childs exceptions
class InvalidParameter(CLIException):
    pass

class UnknownCommand(CLIException):
    pass

class MalformedCommand(CLIException):
    pass

class Documentation(CLIException):
    pass
