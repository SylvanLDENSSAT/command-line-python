import os, json
from ..exc import CLIException
from ..cmd import Command

CONFIG_PATH = os.path.join(os.path.expanduser('~'), '.ngxrc')

class ConfigCMD(Command):
    @staticmethod
    def process(config, *args, **kwargs):
        if kwargs.get('delete'):
            if len(args)>=1:
                for arg in args:
                    config.pop(arg)
                json.dump(config, open(CONFIG_PATH, 'w'))
            else:
                raise CLIException("Nothing to delete")
        elif len(args)==0:
            print(json.dumps(config, indent=4))
        elif kwargs.get('add') and len(args)==2:
            config[args[0]] = args[1]
            json.dump(config, open(CONFIG_PATH, 'w'))
        else:
            return CLIException("<key> <value>")
