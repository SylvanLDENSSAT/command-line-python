from ..cmd import Command
from ..exc import Documentation

class HelpCMD(Command):
    @staticmethod
    def process(config, *args, **kwargs):
        raise Documentation()
